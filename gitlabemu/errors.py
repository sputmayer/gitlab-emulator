"""
Base error types
"""


class GitlabEmulatorError(Exception):
    """
    Common base for all errors we raise
    """
    pass
